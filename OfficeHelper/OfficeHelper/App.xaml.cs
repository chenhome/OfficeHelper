﻿using Hardcodet.Wpf.TaskbarNotification;
using OfficeHelper.Help;
using System;
using System.Windows;

namespace OfficeHelper
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : Application
    {

       

        private TaskbarIcon notifyIcon;
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            //加载默认选择的界面
            var AppearanceManager = WPFTools.Presentation.AppearanceManager.Current;
            AppearanceManager.AccentColor = OfficeHelper.Properties.Settings.Default.SelectedAccentColor;
            AppearanceManager.FontSize = OfficeHelper.Properties.Settings.Default.SelectedFontSize;
            if (OfficeHelper.Properties.Settings.Default.SelectedTheme==0)
            {
                AppearanceManager.ThemeSource = WPFTools.Presentation.AppearanceManager.LightThemeSource;
            }
            else if (OfficeHelper.Properties.Settings.Default.SelectedTheme == 1)
            {
                AppearanceManager.ThemeSource = WPFTools.Presentation.AppearanceManager.DarkThemeSource;
            }
            //加载通知图标
            notifyIcon = (TaskbarIcon) FindResource("NotifyIcon");

            //设置exe路径 并下载exe文件到路径下
            ExeHelp.ExePath = AppDomain.CurrentDomain.BaseDirectory;
            ExeHelp.UpdateExeFile();
        }

        protected override void OnExit(ExitEventArgs e)
        {
            //释放资源
            notifyIcon.Visibility = Visibility.Hidden;
            notifyIcon.Dispose();
            base.OnExit(e);
        }

    }
}
