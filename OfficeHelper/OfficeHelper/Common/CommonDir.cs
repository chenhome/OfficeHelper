﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace OfficeHelper.Common
{
    /// <summary>
    /// 通用的目录
    /// </summary>
    public class CommonDir
    {


        private static readonly string _SettingFullPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory , "Files", "Settings.json");
        /// <summary>
        /// 设置文件目录
        /// </summary>
        public static string SettingFullPath { get { return _SettingFullPath; } }


        private static string _IconDirPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Files", "Icon");

        /// <summary>
        /// 保存的图标文件夹路径
        /// </summary>
        public static string IconDirPath { get { return _IconDirPath; } }



        private static string _SQLGenerateFullExcelPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Files/SQLGenerate/SQLGenerate.xlsx");

        /// <summary>
        /// SQLGenerate 的excel模版路径
        /// </summary>
        public static string SQLGenerateFullExcelPath { get { return _SQLGenerateFullExcelPath; } }


        private static string _ReportGenerateBaseDir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Files/ReportGenerate");

        /// <summary>
        /// 报表文件模版的生成的基础路径
        /// </summary>
        public static string ReportGenerateBaseDir { get { return _ReportGenerateBaseDir; } }
    }
}
