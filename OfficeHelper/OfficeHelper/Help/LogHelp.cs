﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;

namespace OfficeHelper.Help
{

    /// <summary>
    /// 记录日志
    /// </summary>
    public class LogHelp
    {
        private static LogHelp m_Instance;
        /// <summary>
        /// 单例实例
        /// </summary>
        public static LogHelp Instance
        {
            get { return m_Instance ?? (m_Instance = new LogHelp()); }
        }

        /// <summary>
        /// 使用MessageBox显示
        /// </summary>
        public bool IsShowMessageBox { get; set; } = false;

        private bool isShowWPF;
        private LogHelpWPF _wpf;
        /// <summary>
        /// 使用WPF窗体输出
        /// </summary>
        public bool IsShowWPF
        {
            get { return isShowWPF; }
            set { isShowWPF = value;
                if (isShowWPF==true)
                {
                    if (_wpf==null)
                    {
                        _wpf = new LogHelpWPF();
                    }
                    _wpf.Show();
                }
                else
                {
                    if (_wpf != null)
                    {
                        _wpf.Hide();
                    }
                }

            }
        }

        private bool _IsShowConsole;
        /// <summary>
        /// 使用控制台输出
        /// </summary>
        public bool IsShowConsole
        {
            get
            {
                return _IsShowConsole;
            }
            set
            {
                _IsShowConsole = value;
                if (_IsShowConsole)
                {
                    ConsoleManager.Show();
                }
                else
                {
                    ConsoleManager.Hide();
                }
            }
        }

        




        /// <summary>
        /// 记录调试
        /// </summary>
        /// <param name="msg"></param>
        public void Debug(string msg)
        {
            ShowInfo(msg, LogHelpType.Debug);
        }
        /// <summary>
        /// 记录信息
        /// </summary>
        /// <param name="msg"></param>
        public void Info(string msg)
        {
            ShowInfo(msg, LogHelpType.Info);
        }
        /// <summary>
        /// 记录警告
        /// </summary>
        /// <param name="msg"></param>
        public void Waring(string msg)
        {
            ShowInfo(msg, LogHelpType.Waring);

        }
        /// <summary>
        /// 记录错误
        /// </summary>
        /// <param name="msg"></param>
        public void Error(string msg)
        {
            ShowInfo(msg, LogHelpType.Error);
        }

        /// <summary>
        /// 显示或记录信息
        /// </summary>
        /// <param name="info"></param>
        /// <param name="type"></param>
        public void ShowInfo(string info, LogHelpType type)
        {
            if (IsShowMessageBox)
            {
                ShowByMessageBox(info, type);
            }
            if (IsShowConsole)
            {
                ShowByConsole(info,type);
            }

            if (IsShowWPF)
            {
                ShowByWPF(info, type);
            }
        }


        private void ShowByMessageBox(string info, LogHelpType type)
        {
            System.Windows.MessageBox.Show(info, type.ToString());
        }

        private void ShowByConsole(string info, LogHelpType type)
        {
            Console.WriteLine(info);
        }

        private void ShowByWPF(string info, LogHelpType type)
        {
            _wpf.ADD(info);
        }


        //[DllImport("kernel32.dll")]
        //static extern bool FreeConsole();
        //[DllImport("kernel32.dll")]
        //public static extern bool AllocConsole();
    }

    /// <summary>
    /// 信息类型
    /// </summary>
    public enum LogHelpType
    {
        Debug,
        Info,
        Waring,
        Error
    }



    [SuppressUnmanagedCodeSecurity]
    public static class ConsoleManager
    {
        private const string Kernel32_DllName = "kernel32.dll";

        [DllImport(Kernel32_DllName)]
        private static extern bool AllocConsole();

        [DllImport(Kernel32_DllName)]
        private static extern bool FreeConsole();

        [DllImport(Kernel32_DllName)]
        private static extern IntPtr GetConsoleWindow();

        [DllImport(Kernel32_DllName)]
        private static extern int GetConsoleOutputCP();

        private const UInt32 StdOutputHandle = 0xFFFFFFF5;
        [DllImport("kernel32.dll")]
        private static extern IntPtr GetStdHandle(UInt32 nStdHandle);
        [DllImport("kernel32.dll")]
        private static extern void SetStdHandle(UInt32 nStdHandle, IntPtr handle);



        public static bool HasConsole
        {
            get { return GetConsoleWindow() != IntPtr.Zero; }
        }

        /// <summary>
        /// Creates a new console instance if the process is not attached to a console already.
        /// </summary>
        public static void Show()
        {
            //#if DEBUG
            if (!HasConsole)
            {
                AllocConsole();
                InvalidateOutAndError();

                IntPtr defaultStdout = new IntPtr(7);
                IntPtr currentStdout = GetStdHandle(StdOutputHandle);

                if (currentStdout != defaultStdout)
                    // reset stdout
                    SetStdHandle(StdOutputHandle, defaultStdout);

                // reopen stdout
                TextWriter writer = new StreamWriter(Console.OpenStandardOutput(), Encoding.GetEncoding(936))
                { AutoFlush = true };
                Console.SetOut(writer);
                //设置为中文字符
                Console.OutputEncoding = Encoding.GetEncoding(936);

            }
            //#endif
        }

        /// <summary>
        /// If the process has a console attached to it, it will be detached and no longer visible. Writing to the System.Console is still possible, but no output will be shown.
        /// </summary>
        public static void Hide()
        {
            //#if DEBUG
            if (HasConsole)
            {
                SetOutAndErrorNull();
                FreeConsole();
            }
            //#endif
        }

        public static void Toggle()
        {
            if (HasConsole)
            {
                Hide();
            }
            else
            {
                Show();
            }
        }

        static void InvalidateOutAndError()
        {
            Type type = typeof(System.Console);

            System.Reflection.FieldInfo _out = type.GetField("_out",
                System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic);

            System.Reflection.FieldInfo _error = type.GetField("_error",
                System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic);

            System.Reflection.MethodInfo _InitializeStdOutError = type.GetMethod("InitializeStdOutError",
                System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic);

            Debug.Assert(_out != null);
            Debug.Assert(_error != null);

            Debug.Assert(_InitializeStdOutError != null);

            _out.SetValue(null, null);
            _error.SetValue(null, null);

            _InitializeStdOutError.Invoke(null, new object[] { true });
        }

        static void SetOutAndErrorNull()
        {
            Console.SetOut(TextWriter.Null);
            Console.SetError(TextWriter.Null);
        }
    }

}
