﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace OfficeHelper.Help
{
    /// <summary>
    /// LogHelpWPF.xaml 的交互逻辑
    /// </summary>
    public partial class LogHelpWPF : Window
    {
        public LogHelpWPF()
        {
            InitializeComponent();
        }

        public void ADD(string msg)
        {
            //listBox.Items.Add(msg);
            //listBox.ScrollIntoView(listBox.Items[listBox.Items.Count - 1]);

            listBox.AppendText(msg+Environment.NewLine);
            listBox.ScrollToEnd();
            //listBox.ScrollIntoView
            //ScrollIntoView
            //scroll
            //listBox.Items.MoveCurrentToLast();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //关闭窗体时隐藏
            e.Cancel = true;
            this.Hide();
        }
    }
}
