﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace OfficeHelper.Help
{
    /// <summary>
    /// 桌边快捷方式创建类
    /// 需要需要引入COM组件 IWshRuntimeLibrary。在添加引用对话框中搜索Windows Script Host Object Model，选择之后添加到Project的引用中。
    /// </summary>
    public class Shortcut
    {

        /// <summary>
        /// 创建快捷方式
        /// </summary>
        /// <param name="directory">快捷方式路径</param>
        /// <param name="shortcutName">快捷方式名称</param>
        /// <param name="targetPath">应用程序原始路径</param>
        /// <param name="description">设置备注</param>
        /// <param name="iconLocation">设置图标</param>
        /// <returns></returns>
        public static bool CreateShortcut(string directory, string shortcutName, string targetPath, string description = null, string iconLocation = null)
        {
            try
            {
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }
                //添加引用 Com 中搜索 Windows Script Host Object Model
                string shortcutPath = Path.Combine(directory, string.Format("{0}.lnk", shortcutName));
                IWshRuntimeLibrary.WshShell shell = new IWshRuntimeLibrary.WshShell();
                IWshRuntimeLibrary.IWshShortcut shortcut = (IWshRuntimeLibrary.IWshShortcut)shell.CreateShortcut(shortcutPath);//创建快捷方式对象
                shortcut.TargetPath = targetPath;//指定目标路径
                shortcut.WorkingDirectory = Path.GetDirectoryName(targetPath);//设置起始位置
                shortcut.WindowStyle = 1;//设置运行方式，默认为常规窗口
                shortcut.Description = description;//设置备注
                shortcut.IconLocation = string.IsNullOrWhiteSpace(iconLocation) ? targetPath : iconLocation;//设置图标路径
                shortcut.Save();//保存快捷方式
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.ToString());
            }
            return false;
        }


        /// <summary>
        /// 创建桌面快捷方式
        /// </summary>
        /// <param name="shortcutName">快捷方式名称</param>
        /// <param name="targetPath">目标路径</param>
        /// <param name="description">描述</param>
        /// <param name="iconLocation">图标路径，格式为"可执行文件或DLL路径, 图标编号"</param>
        /// <remarks></remarks>
        public static void CreateShortcutOnDesktop(string shortcutName, string targetPath,
            string description = null, string iconLocation = null)
        {
            string desktop = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);//获取桌面文件夹路径
            CreateShortcut(desktop, shortcutName, targetPath, description, iconLocation);
        }

        /// <summary>
        /// 获取快捷方式的目标路径
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string GetCreateShortcutTargetPath(string path)
        {
            IWshRuntimeLibrary.IWshShortcut _shortcut = null;
            IWshRuntimeLibrary.IWshShell_Class shell = new IWshRuntimeLibrary.IWshShell_Class();
            if (System.IO.File.Exists(path) == true)
                _shortcut = shell.CreateShortcut(path) as IWshRuntimeLibrary.IWshShortcut;//在vs2010中CreateShortcut返回dynamic 类型
            //所以要加as 进行对象类型转换       
             return _shortcut.TargetPath;
        }
    }
}
