﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace OfficeHelper.Model
{
    /// <summary>
    /// 快速启动目标模型
    /// </summary>
    public class FastTargetModel: ObservableObject 
    {
        private string _Name;

        /// <summary>
        /// 项目名称
        /// </summary>
        public string Name
        {
            get { return _Name; }
            set
            {
                Set("Name", ref _Name, value);
            }
        }

        private string _Alias;

        /// <summary>
        /// 别名 用户可以修改为其他 以空格分隔
        /// </summary>
        public string Alias
        {
            get { return _Alias; }
            set
            {
                Set("Alias", ref _Alias, value);
            }
        }

        private string _Target;
        /// <summary>
        /// 项目路径
        /// </summary>
        public string Target
        {
            get { return _Target; }
            set
            {
                Set("Target", ref _Target, value);
            }
        }

        private string _Param;
        /// <summary>
        /// 执行 参数 参数设置
        /// </summary>
        public string Param
        {
            get { return _Param; }
            set
            {
                Set("Param", ref _Param, value);
            }
        }

        private string _ICON;
        /// <summary>
        /// 图标
        /// </summary>
        public string ICON
        {
            get { return _ICON; }
            set
            {
                Set("ICON", ref _ICON, value);
            }
        }

        private bool _TargetInvalid = false;
        /// <summary>
        /// 目标是否无效 无效为true
        /// </summary>
        public bool TargetInvalid
        {
            get { return _TargetInvalid; }
            set
            {
                Set("TargetInvalid", ref _TargetInvalid, value);
            }
        }

        /// <summary>
        /// 验证目标是否无效
        /// </summary>
        /// <returns></returns>
        public bool ValidateTargetInvalid()
        {
            //为空 表示无效
            if (Target==null)
            {
                TargetInvalid = true;
                return TargetInvalid;
            }

            
            if (File.Exists(Target)) //表示为文件
            {
                TargetInvalid = false;
            }
            else if (Directory.Exists(Target))
            {
                TargetInvalid = false; //表示为 目录
            }else
            {
                TargetInvalid = true;
            }

            return TargetInvalid;
        }

    }
}
