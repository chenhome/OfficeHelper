﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace OfficeHelper.Model.SQLGenerate
{

    /// <summary>
    /// 生成字段的对象
    /// </summary>
    public class SQLGenerateField: ObservableObject
    {
        private string _Name;

        /// <summary>
        /// 字段名称
        /// </summary>
        public string Name
        {
            get { return _Name; }
            set
            {
                Set("Name", ref _Name, value);
            }
        }

        private string _Type;

        /// <summary>
        /// 字段类型
        /// </summary>
        public string Type
        {
            get { return _Type; }
            set
            {
                Set("Type", ref _Type, value);
            }
        }

        private string _Length;

        /// <summary>
        /// 字段长度
        /// </summary>
        public string Length
        {
            get { return _Length; }
            set
            {
                Set("Length", ref _Length, value);
            }
        }


        private bool _EnableNull;

        /// <summary>
        /// 是否可为NULL    
        /// </summary>
        public bool EnableNull
        {
            get { return _EnableNull; }
            set
            {
                Set("EnableNull", ref _EnableNull, value);
            }
        }


        private bool _IsKey;

        /// <summary>
        /// 是否表示为主键
        /// </summary>
        public bool IsKey
        {
            get { return _IsKey; }
            set
            {
                Set("IsKey", ref _IsKey, value);
            }
        }

        private string _Remark;
        /// <summary>
        /// 备注说明
        /// </summary>
        public string Remark
        {
            get { return _Remark; }
            set
            {
                Set("Remark", ref _Remark, value);
            }
        }

        private int _index;
        /// <summary>
        /// 序号
        /// </summary>
        public int Index
        {
            get { return _index; }
            set
            {
                Set("Index", ref _index, value);
            }
        }

        private string _defaultValue;
        /// <summary>
        /// 默认值
        /// </summary>
        public string DefaultValue
        {
            get { return _defaultValue; }
            set
            {
                Set("DefaultValue", ref _defaultValue, value);
            }
        }


    }
}
