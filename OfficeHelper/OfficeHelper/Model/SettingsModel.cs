﻿using GalaSoft.MvvmLight;
using Newtonsoft.Json;
using OfficeHelper.Help;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;

namespace OfficeHelper.Model
{
    public class SettingsModel: ObservableObject
    {

        /// <summary>
        /// 设置文件的保存路径
        /// </summary>
        private static string settingFullPath = Common.CommonDir.SettingFullPath;

        private static SettingsModel _settingsModel;

        public static SettingsModel Default
        {
            get {
                if (_settingsModel == null)
                {
                    SettingsModel.LoadFromJson();
                    //验证目标是否有效
                    _settingsModel.ValidateTargetInvalid();
                }
                return _settingsModel;
            }
        }

        public SettingsModel()
        {
            //路径不存在则创建路径
            DirectoryInfo dirIconPath = new DirectoryInfo(IconPath);
            if (!dirIconPath.Exists)
            {
                dirIconPath.Create();
            }
        }

        private long _BaiduTranAppid;//= 20171231000110820;

        /// <summary>
        /// 百度翻译Appid
        /// </summary>
        public long BaiduTranAppid
        {
            get { return _BaiduTranAppid; }
            set
            {
                Set("BaiduTranAppid", ref _BaiduTranAppid, value);
            }
        }


        private string _BaiduTranKey;//= "pg29toxupL0pP1V52rn4";
        /// <summary>
        /// 百度翻译密钥
        /// </summary>
        public string BaiduTranKey
        {
            get { return _BaiduTranKey; }
            set
            {
                Set("BaiduTranKey", ref _BaiduTranKey, value);
            }
        }


        private string _LastOpenView;

        /// <summary>
        /// 上次打开的View
        /// </summary>
        public string LastOpenView
        {
            get { return _LastOpenView; }
            set
            {
                Set("LastOpenView", ref _LastOpenView, value);
            }
        }


        private string _SrcLangue;
        /// <summary>
        /// 原语言
        /// </summary>
        public string SrcLangue
        {
            get { return _SrcLangue; }
            set
            {
                Set("SrcLangue", ref _SrcLangue, value);
            }
        }

        private string _DstLangue;
        /// <summary>
        /// 目标语言
        /// </summary>
        public string DstLangue
        {
            get { return _DstLangue; }
            set
            {
                Set("DstLangue", ref _DstLangue, value);
            }
        }


        private bool _IsCloseMinimized;
        /// <summary>
        /// 关闭时最小化不关闭
        /// </summary>
        public bool IsCloseMinimized
        {
            get { return _IsCloseMinimized; }
            set
            {
                Set("IsCloseMinimized", ref _IsCloseMinimized, value);
            }
        }

        private string _FileFilterSelectedDir;
        /// <summary>
        /// 文件过滤选择的dir
        /// </summary>
        public string FileFilterSelectedDir
        {
            get { return _FileFilterSelectedDir; }
            set
            {
                Set("FileFilterSelectedDir", ref _FileFilterSelectedDir, value);
            }
        }


        private string _VSSolutionSelectSolutionFile;
        /// <summary>
        /// VSSolution 选择的解决方案文件
        /// </summary>
        public string VSSolutionSelectSolutionFile
        {
            get { return _VSSolutionSelectSolutionFile; }
            set
            {
                Set("VSSolutionSelectSolutionFile", ref _VSSolutionSelectSolutionFile, value);
            }
        }

        private string _VSSolutionSearchText;
        /// <summary>
        /// VS解决方案查找
        /// </summary>
        public string VSSolutionSearchText
        {
            get { return _VSSolutionSearchText; }
            set
            {
                Set("VSSolutionSearchText", ref _VSSolutionSearchText, value);
            }
        }




        public List<HotKeyModel> HotKeys { get; set; }
        /// <summary>
        /// 快捷方式信息列表
        /// </summary>
        public ObservableCollection<FastTargetModel> FastTargetList { get; set; } = new ObservableCollection<FastTargetModel>();

        private string _IconPath = Common.CommonDir.IconDirPath;
        /// <summary>
        /// 图标路径
        /// </summary>
        public string IconPath
        {
            get { return _IconPath; }
        }

        /// <summary>
        /// 插入目标快捷方式
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool FastTargetAdd(FastTargetModel model)
        {
            //目标和参数不相等才添加
            var get = from target in FastTargetList
                      where target.Target == model.Target && target.Param == model.Param
                      select target;
            if (get.Count() == 0  )
            {
                FastTargetList.Add(model);
                return true;
            }
            return false;
        }

        public  static void LoadFromJson()
        {
            try
            {
                //文件存在才读取
                if (File.Exists(settingFullPath))
                {
                    using (FileStream fs = new FileStream(settingFullPath, FileMode.Open, FileAccess.Read))
                    {
                        StreamReader sr = new StreamReader(fs, Encoding.UTF8);
                        string json = sr.ReadToEnd();
                        _settingsModel = JsonConvert.DeserializeObject<SettingsModel>(json);
                        sr.Close();
                    }
                }

                if (_settingsModel==null)
                {
                    _settingsModel = new SettingsModel();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        /// <summary>
        /// 移除无效的路径
        /// </summary>
        private void ValidateTargetInvalid()
        {
            if (FastTargetList!=null)
            {
                //遍历循环 路径
                foreach (var item in FastTargetList)
                {
                     item.ValidateTargetInvalid(); //验证路径是否有效
                }
            }
        }

        /// <summary>
        /// 保存信息
        /// </summary>
        public void Save()
        {
            try
            {
                string json = JsonConvert.SerializeObject(_settingsModel, Formatting.Indented);
                System.IO.File.WriteAllText(settingFullPath, json, Encoding.UTF8);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
