﻿using ICSharpCode.AvalonEdit.Highlighting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OfficeHelper.View.Editer
{
    /// <summary>
    /// EditerContol.xaml 的交互逻辑
    /// </summary>
    public partial class EditerContol : UserControl
    {
        public EditerContol()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 表示是否是远程文件
        /// </summary>
        public bool IsRemote { get; set; }

        //public IoSession Session { get; set; }

        private string currentFileName;
        private string currentRemoteFileName;


        private void openFileClick(object sender, RoutedEventArgs e)
        {
            //OpenFileDialog dlg = new OpenFileDialog();
            //dlg.CheckFileExists = true;
            //if (dlg.ShowDialog() == DialogResult.OK)
            //{
            //    OpenFile(dlg.FileName);
            //    //currentFileName = dlg.FileName;
            //    //textEditor.Load(currentFileName);
            //    //textEditor.SyntaxHighlighting = HighlightingManager.Instance.GetDefinitionByExtension(System.IO.Path.GetExtension(currentFileName));
            //}
        }


        /// <summary>
        /// 设置文件名称
        /// </summary>
        /// <param name="fileName"></param>
        public bool OpenFile(string fileName)
        {
            try
            {
                System.IO.FileInfo fileInfo = new System.IO.FileInfo(fileName);
                if (fileInfo.Exists)
                {
                    string[] cannotOpen = { "doc", "docx", "xls", "xlsx", "dll", "exe", "pdb", "ppt" };
                    foreach (var item in cannotOpen)
                    {
                        if (fileInfo.Extension.ToLower().Contains(item))
                        {
                            return false;
                        }
                    }
                    currentFileName = fileName;

                    //构造读取文件流对象读取可以使用的对象
                    using (FileStream fsRead = new FileStream(currentFileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        textEditor.Load(fsRead);
                    }

                    SetHighlighting(currentFileName);
                    return true;
                }
                return false;
            }
            catch (System.Exception ex)
            {
                System.Windows.MessageBox.Show(ex.ToString());
                return false;
            }
        }

        /// <summary>
        /// 根据文件名称 设置代码格式
        /// </summary>
        /// <param name="filename"></param>
        public void SetHighlighting(string filename)
        {
            try
            {
                if (!string.IsNullOrEmpty(filename))
                {
                    string ext = System.IO.Path.GetExtension(filename);
                    textEditor.SyntaxHighlighting = HighlightingManager.Instance.GetDefinitionByExtension(ext);
                    if (ext.Contains("json") || ext.Contains("txt"))
                    {
                        textEditor.SyntaxHighlighting = HighlightingManager.Instance.GetDefinitionByExtension(".cs");
                    }
                }
            }
            catch (System.Exception)
            {

                throw;
            }

        }


        public void OpenRemoteFile(string romoteFilePath)
        {
            if (!string.IsNullOrEmpty(romoteFilePath))
            {
                currentRemoteFileName = romoteFilePath;
                SetHighlighting(romoteFilePath);
                //if (Session != null)
                //{
                //    TransferObj obj = new TransferObj()
                //    {
                //        Type = TranType.ReadRemoteFile,
                //        FileFullPath = romoteFilePath
                //    };
                //    Session.Write(obj);
                //}
            }
        }

        public void SaveRemoteFile(string romoteFilePath)
        {
            if (!string.IsNullOrEmpty(romoteFilePath))
            {
                currentRemoteFileName = romoteFilePath;
                SetHighlighting(romoteFilePath);
                //if (Session != null)
                //{
                //    TransferObj obj = new TransferObj()
                //    {
                //        Type = TranType.SaveRemoteFile,
                //        FileFullPath = romoteFilePath,
                //        FileContent = textEditor.Text
                //    };
                //    Session.Write(obj);
                //}
            }
        }


        private void saveFileClick(object sender, RoutedEventArgs e)
        {
            //远程文件
            if (IsRemote)
            {
                SaveRemoteFile(currentRemoteFileName);
            }
            else
            {
                if (currentFileName == null)
                {
                    //SaveFileDialog dlg = new SaveFileDialog();
                    //dlg.DefaultExt = ".txt";
                    //if (dlg.ShowDialog() == DialogResult.OK)
                    //{
                    //    currentFileName = dlg.FileName;
                    //}
                    //else
                    //{
                    //    return;
                    //}
                }
                textEditor.Save(currentFileName);
            }
        }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            //远程文件
            if (IsRemote)
            {
                OpenRemoteFile(currentRemoteFileName); //刷新重新加载文件
                //RefreshAction.Invoke();
            }
            else
            {
                if (currentFileName != null)
                {
                    OpenFile(currentFileName);
                }
            }
        }
    }
}
