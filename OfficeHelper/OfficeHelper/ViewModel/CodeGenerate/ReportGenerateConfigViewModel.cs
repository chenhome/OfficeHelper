﻿using GalaSoft.MvvmLight.Command;
using RazorEngine;
using RazorEngine.Templating;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace OfficeHelper.ViewModel.CodeGenerate
{
    /// <summary>
    /// 报告单生成
    /// </summary>
    public class ReportGenerateConfigViewModel : OfficeHelpViewModelBase
    {
        public ReportGenerateConfigViewModel()
        {
            //路径不存在则新建
            CreateDir(BaseDir);
            GenerateCommand = new RelayCommand(GenerateMethod);
            LoadTemplateCommand = new RelayCommand(LoadTemplateMethod);
            LoadTemplateMethod();
        }

        /// <summary>
        /// 基础路径
        /// </summary>
        private string BaseDir = Common.CommonDir.ReportGenerateBaseDir;

        #region 命令

        public ICommand GenerateCommand { get; set; }

        /// <summary>
        /// 生成命令
        /// </summary>
        private void GenerateMethod()
        {
            //获取文件夹下所有的的cshtml文件

            if (SelectedTemplate!=null)
            {
               var files = SelectedTemplate.GetFiles("*.tmp");
                if (files!=null)
                {
                    foreach (var item in files)
                    {
                        //文件存在
                        if (item.Exists)
                        {
                            string template = File.ReadAllText(item.FullName);
                            //string template = "Hello @Model.Name, welcome to RazorEngine!";
                            //var model = ;
                            // Engine.Razor.RunCompile(template, "templateKey2", null, new { FirstName = "Bill", LastName = "Gates" });
                            var result = Engine.Razor.RunCompile(template, "templateKey", null, new { Name = "World" });
                            ShowMessageBox(result);

                        }
                        //ShowMessageBox(item.FullName);
                    }

                }
            }

            //var path = AppDomain.CurrentDomain.BaseDirectory + "templates\\hello.cshtml";
            //var template = File.ReadAllText(path);
            //var model = new UserInfo { FirstName = "Bill", LastName = "Gates" };
            //var result = Engine.Razor.RunCompile(template, "templateKey2", null, model);

           // ShowMessageBox(MethodName + ControllerName+SelectedTemplate.FullName);
        }

        public ICommand LoadTemplateCommand { get; set; }

        /// <summary>
        /// 加载模版
        /// </summary>
        private void LoadTemplateMethod()
        {
           var  TemplatesDirInfos = new ObservableCollection<System.IO.DirectoryInfo>();
            System.IO.DirectoryInfo directoryInfo = new System.IO.DirectoryInfo(BaseDir);
            var list = directoryInfo.EnumerateDirectories();
            if (list!=null)
            {
                foreach (var item in list)
                {
                    TemplatesDirInfos.Add(item);
                }
            }

            Templates = TemplatesDirInfos;
        }


        #endregion

        #region 字段
        private ObservableCollection<System.IO.DirectoryInfo> _Templates;

        /// <summary>
        /// 模版列表
        /// </summary>
        public ObservableCollection<System.IO.DirectoryInfo> Templates
        {
            get { return _Templates; }
            set
            {
                Set("Templates", ref _Templates, value);
            }
        }

        private System.IO.DirectoryInfo _SelectedTemplate;

        /// <summary>
        /// 选中的模版
        /// </summary>
        public System.IO.DirectoryInfo SelectedTemplate
        {
            get { return _SelectedTemplate; }
            set
            {
                Set("SelectedTemplate", ref _SelectedTemplate, value);
            }
        }



        private string _ControllName;

        /// <summary>
        /// 控制器名称
        /// </summary>
        public string ControllerName
        {
            get { return _ControllName; }
            set
            {
                Set("ControllerName", ref _ControllName, value);
            }
        }

        private string _MethodName;

        /// <summary>
        /// 方法名
        /// </summary>
        public string MethodName
        {
            get { return _MethodName; }
            set
            {
                Set("MethodName", ref _MethodName, value);
            }
        } 
        #endregion

    }
}
